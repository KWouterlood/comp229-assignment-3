package demo;

import java.util.Random;

import lib.Counter;

// Purpose of demo.Main is to show that game WAS NOT 
// threadsafe.

public class Main {
	// private static Counter counter = new Counter();
	// Counter can be accessed as methods are static
	private static int dec;

	public static void main(String[] args) {
		dec = 0;
		demo();

	}

	public static void demo() {
		// set counter to random value
		Random rand = new Random();
		int randInt = rand.nextInt(10);
		Counter.set(randInt);
		System.out.println("Counter set to " + randInt);
		
		class demoThread implements Runnable {
			int value;
			String name;

			demoThread(String name, int val) {
				this.name = name;
				value = val;
			}

			public void run() {
				// deplete counter while keeping track of how
				// many times the counter has been depleted
				while (!Counter.depleted()) {
					Counter.decrement();
					value--;
					// increment decrement counter
					dec++;
					System.out.println("Counter Decremented by " + name + ". Value: " + value);
				}
				System.out.println(name + " counter Depleted!");
			}
		}
		// create three new threads.
		Thread A = new Thread(new demoThread("Thread A", randInt));
		Thread B = new Thread(new demoThread("Thread B", randInt));
		Thread C = new Thread(new demoThread("Thread C", randInt));

		// run threads
		A.start();
		B.start();
		C.start();

		while (A.isAlive() || B.isAlive() || C.isAlive()) {
			// do nothing
		}

		// check to see if the counter matches expected value
		if (dec > randInt) {

			// if counter does not match
			System.out.println("\nCounter was decremented at least " + dec + " times!");
			System.out.println("Counter is not thread safe!");
		} else {

			System.out.println("Counter is thread safe..");
		}
	}

}
