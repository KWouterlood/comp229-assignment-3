package behaviours;

import main.*;
import onscreen.*;

public class Passive implements Behaviour {
	
	// passive behaviour doesn't move
	public Cell execute(Cell location) {
		return location;
	}
}