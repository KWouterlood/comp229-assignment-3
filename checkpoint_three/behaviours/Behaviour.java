package behaviours;

import main.*;
import onscreen.*;

// interface for character behaviours
public interface Behaviour {

	public Cell execute(Cell location);

}