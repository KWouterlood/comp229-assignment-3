package behaviours;

import main.*;
import onscreen.*;

public class Chase implements Behaviour {
	
	onscreen.Character target;
	
	// select the character to chase
	public Chase(onscreen.Character target) {
		this.target = target;
	}

	public Cell execute(Cell location) {
		
		// return the cell that is one closer to the target from current position
		return Stage.getInstance().oneCellCloserTo(location, target.getLocation());
	}

}