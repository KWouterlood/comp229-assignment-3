package main;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

import behaviours.Chase;
import behaviours.Passive;
import onscreen.*;
import lib.Counter;

public class Stage extends javax.swing.JPanel implements MouseListener, MouseMotionListener {
	// get instance of the stage
	// uses singleton pattern
	private static Stage instance = new Stage();

	public Grid grid;
	public onscreen.Character sheep;
	public onscreen.Character wolf;
	public onscreen.Character shepherd;
	public onscreen.Teleporters teleporters;
	public List<onscreen.Character> rabbitHerd;

	public boolean readyToStep() {
		return CounterAdaptor.depleted();
	}

	public boolean sheepCaught = false;

	Point lastMouseLoc = new Point(0, 0);

	List<MouseObserver> observers = new ArrayList<MouseObserver>();

	public Stage() {
		// populate stage ("game board")
		grid = new Grid();
		for (Cell c : grid){
			registerMouseObserver(c);
		}
		teleporters = new Teleporters(grid.giveMeRandomCell(), grid.giveMeRandomCell());
		shepherd = new Shepherd(grid.giveMeRandomCell(), new Passive());
		// sheep must be created first so that the wolf can chase it.
		// this can be avoided by reinstating the old behaviours
		sheep = new Sheep(grid.giveMeRandomCell(), new Chase(shepherd));
		wolf = new Wolf(grid.giveMeRandomCell(), new Chase(sheep));
		// if the sheep spawns within sight of the wolf
		// spawn sheep in a new location
		while (sheep.canSee(wolf)) {
			sheep.setLocation(grid.giveMeRandomCell());
		}
		// create a "herd" of rabbits to run wild
		rabbitHerd = new ArrayList<onscreen.Character>(10);
		for (int i = 0; i < 10; i++) {
			rabbitHerd.add(i, new RabbitAdaptor(grid.giveMeRandomCell()));
		}

		registerMouseObserver(shepherd);
		addMouseListener(this);
		addMouseMotionListener(this);
	}

	public static Stage getInstance() {
		return instance;
	}

	public void paint(Graphics g) {
		draw(g);
	}
	// draw game components
	public void draw(Graphics g) {
		grid.draw(g);
		sheep.draw(g);
		wolf.draw(g);
		shepherd.draw(g);
		teleporters.draw(g);
		for (onscreen.Character rabbit : rabbitHerd) {
			rabbit.draw(g);
		}
		if (result()) {
			g.setColor(Color.BLACK);
			g.setFont(new Font(Font.DIALOG, Font.BOLD, 36));
			g.drawString("Game Over!", 200, 200);
		}
	}
	
	// step through game
	public void step() {
		CounterAdaptor.set(2 + rabbitHerd.size());
		new Thread(sheep).start();
		new Thread(wolf).start();
		for (onscreen.Character rabbit : rabbitHerd) {
			new Thread(rabbit).start();
		}
		if (!sheepCaught && sheep.getLocation() == shepherd.getLocation()) {
			shepherd = new SheepCarrier(shepherd);
		}

	}

	// control the teleportation of characters
	public Cell checkTeleport(Cell location) {
		if (location.equals(teleporters.getLocationOne())) {
			return teleporters.getLocationTwo();
		} else if (location.equals(teleporters.getLocationTwo())) {
			return teleporters.getLocationOne();
		} else {
			return location;
		}

	}

	public void registerMouseObserver(MouseObserver mo) {
		observers.add(mo);
	}
	
	// return a cell one cell closer to the target cell
	public Cell oneCellCloserTo(Cell from, Cell to) {
		int xdiff = to.x - from.x;
		int ydiff = to.y - from.y;
		int newX = from.x + Integer.signum(xdiff);
		int newY = from.y + Integer.signum(ydiff);
		if (cellOccupied(newX, newY))
			return from; // i..e you can't move directly closer, so stay still.
		return grid.getCell(newX, newY);
	}
	
	// return adjacent cells to the current cell
	public Cell getAdjacent(Cell cell, Direction direction) {
		int newX = cell.x + direction.dx;
		int newY = cell.y + direction.dy;
		if (cellOccupied(newX, newY))
			return cell; // i.e. the adjacent cell in that direction is this
							// very cell (that's a modelling problem)
		try {
			return grid.getCell(newX, newY);
		} catch (ArrayIndexOutOfBoundsException e) {
			return cell; // if the adjacency is off the grid, just return the
							// original cell.
		}
	}
	
	// check to see if cell is occupied
	public boolean cellOccupied(int x, int y) {
		for (onscreen.Character rabbit : rabbitHerd) {
			if (rabbit.getLocation().x == x && rabbit.getLocation().y == y)
				return true;
		}
		return false;
	}

	// implementation of MouseListener and MouseMotionListener
	public void mouseClicked(MouseEvent e) {
		if (shepherd.getBounds().contains(e.getPoint())) {
			shepherd.mouseClicked(e);
		}
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseDragged(MouseEvent e) {
	}

	public void mouseMoved(MouseEvent e) {
		for (MouseObserver mo : observers) {
			Rectangle bounds = mo.getBounds();
			if (bounds.contains(e.getPoint())) {
				mo.mouseEntered(e);
			} else if (bounds.contains(lastMouseLoc)) {
				mo.mouseLeft(e);
			}
		}
		lastMouseLoc = e.getPoint();
	}

	// check to see if the game has a final result yet
	public boolean result() {
		if (shepherd.getLocation().equals(wolf.getLocation())) {
			return true;
		} else if (wolf.getLocation().equals(sheep.getLocation())) {
			return true;
		} else if (shepherd.getLocation().equals(sheep.getLocation())) {
			return true;
		} else {
			return false;
		}
	}
}
