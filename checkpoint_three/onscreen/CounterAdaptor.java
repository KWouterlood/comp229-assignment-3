package onscreen;

public class CounterAdaptor {
// synchronized to be thread safe
	
	public static synchronized void set(int i) {
		lib.Counter.set(i);
	}

	public static synchronized boolean depleted() {
		return lib.Counter.depleted();
	}

	public static synchronized void decrement() {
		lib.Counter.decrement();
	}

}
