package onscreen;

import java.awt.*;

// class to give visual indicator to the shepherd carrying a sheep
public class SheepCarrier extends CharacterDecorator {
	public SheepCarrier(Character c) {
		super(c);
	}

	@Override
	public void draw(Graphics g) {
		character.draw(g);
		Rectangle r = character.getBounds();
		g.setColor(Color.WHITE);
		g.fillOval(r.x + 5, r.y + 5, 25, 25);
	}
}
