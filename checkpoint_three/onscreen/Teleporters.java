package onscreen;

import java.awt.Color;
import java.awt.Graphics;

public class Teleporters {
	private Cell locationOne;
	private Cell locationTwo;
	private Color backColour;
	private Color portalColour;
	
	// spawn teleporters
	public Teleporters(Cell firstLoc, Cell secondLoc) {
		locationOne = firstLoc;
		locationTwo = secondLoc;
		backColour = new java.awt.Color(144, 225, 252);
		portalColour = new java.awt.Color(5, 112, 252);
	}
	
	public Cell getLocationOne() {
		return locationOne;
	}

	public Cell getLocationTwo() {
		return locationTwo;
	}
	
	// draw teleporters on game board
	public void draw(Graphics g) {
		g.setColor(backColour);
		g.fillRect(locationOne.getTopLeft().x, locationOne.getTopLeft().y, 35, 35);
		g.fillRect(locationTwo.getTopLeft().x, locationTwo.getTopLeft().y, 35, 35);
		g.setColor(portalColour);
		g.fillOval(locationOne.getTopLeft().x + 6, locationOne.getTopLeft().y, 20, 35);
		g.fillOval(locationTwo.getTopLeft().x + 6, locationTwo.getTopLeft().y, 20, 35);
	}
}
